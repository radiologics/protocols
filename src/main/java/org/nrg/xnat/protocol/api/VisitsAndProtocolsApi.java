package org.nrg.xnat.protocol.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.nrg.action.ClientException;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatPvisitdata;
import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xdat.om.base.auto.AutoXnatPvisitdata;
import org.nrg.xdat.schema.SchemaElement;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xft.schema.Wrappers.GenericWrapper.GenericWrapperElement;
import org.nrg.xnat.protocol.entities.subentities.VisitType;
import org.nrg.xnat.protocol.services.VisitTypeService;
import org.nrg.xnat.protocol.services.impl.ProtocolLabeler;
import org.nrg.xnat.protocol.util.ProtocolVisitSubjectHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Nullable;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@XapiRestController
@RequestMapping(value = "/protocols")
@Api("Visits API")
public class VisitsAndProtocolsApi extends AbstractXapiRestController {
    private final VisitTypeService visitTypeService;
    private final ProtocolLabeler labelingService;

    @Autowired
    public VisitsAndProtocolsApi(VisitTypeService visitTypeService,
                                 ProtocolLabeler labelingService,
                                 UserManagementServiceI userManagementService,
                                 RoleHolder roleHolder) {
        super(userManagementService, roleHolder);
        this.visitTypeService = visitTypeService;
        this.labelingService = labelingService;
    }


    @ApiOperation(value = "Generate experiment label")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Invalid parameters"),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "generate_label", method = POST)
    public ResponseEntity<String> generateLabel(final @RequestParam(required = false) String project,
                                                final @RequestParam(required = false) String subject,
                                                final @RequestParam(required = false) String visitid,
                                                final @RequestParam(required = false) String datatype,
                                                final @RequestParam(required = false) String modality,
                                                final @RequestParam(required = false) String date,
                                                final @RequestParam(required = false) String dateFormat,
                                                final @RequestParam(required = false) String subtype) {
        try {
            String label = labelingService.generateLabel(project, subject, visitid, subtype, datatype, modality,
                    date, dateFormat, getSessionUser());
            return new ResponseEntity<>(label, HttpStatus.OK);
        } catch (ClientException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getClass().getName() + ": " + e.getMessage(),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Get datatypes for visit")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Invalid parameters"),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "/projects/{projectId}/visits/{visitId}/datatypes", method = GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<XnatDatatype>> getDatatypes(final @PathVariable String projectId,
                                                           final @PathVariable String visitId,
                                                           final @RequestParam(required = false) Boolean imagingOnly) {
        try {
            VisitType vt = getVisitType(projectId, visitId);
            Collection<String> typesForVisit;
            if (vt == null) {
                // Ad hoc visit, allow any datatype
                typesForVisit = makeTypesForVisitList();
            } else {
                typesForVisit = vt.getDatatypesForVisit();
            }

            List<XnatDatatype> datatypes = new ArrayList<>();
            for (String type : typesForVisit) {
                SchemaElement se;
                try {
                    se = SchemaElement.GetElement(type);
                } catch (XFTInitException | ElementNotFoundException e) {
                    continue;
                }
                if (imagingOnly != null && imagingOnly &&
                        !se.instanceOf(XnatImagesessiondata.SCHEMA_ELEMENT_NAME)) {
                    continue;
                }
                datatypes.add(new XnatDatatype(se, type));
            }

            return new ResponseEntity<>(datatypes, HttpStatus.OK);
        } catch (ClientException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Get subtypes for visit and datatype")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Invalid parameters"),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "/projects/{projectId}/visits/{visitId}/datatypes/{datatype}/subtypes", method = GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Set<String>> getSubtypes(final @PathVariable String projectId,
                                                   final @PathVariable String visitId,
                                                   final @PathVariable String datatype) {
        try {
            VisitType vt = getVisitType(projectId, visitId);
            return new ResponseEntity<>(vt == null ? new HashSet<String>() : vt.getSubtypesForDatatype(datatype),
                    HttpStatus.OK);
        } catch (ClientException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Return visit type or null if ad hoc
     * @param projectId project
     * @param visitId visit
     * @return visit type object or null if ad hoc
     * @throws ClientException if visitId / projectId invalid
     */
    @Nullable
    private VisitType getVisitType(String projectId, String visitId) throws ClientException {
        XnatPvisitdata visit = AutoXnatPvisitdata.getXnatPvisitdatasById(visitId, getSessionUser(), false);
        if (visit == null) {
            throw new ClientException("No visit with id " + visitId);
        }
        if (visit.getVisitType() == null) {
            // ad hoc visit
            return null;
        }
        VisitType vt = visitTypeService.getVisitType(Long.parseLong(visit.getProtocolid()),
                visit.getVisitType(), projectId);
        if (vt == null) {
            throw new ClientException("No visit with id " + visitId + " associated with project " + projectId);
        }
        return vt;
    }

    private List<String> makeTypesForVisitList() throws XFTInitException, ElementNotFoundException {
        List<String> typesForVisit = new ArrayList<>();
        for (GenericWrapperElement gwe : GenericWrapperElement.GetAllElements(false)) {
            String type = gwe.getXSIType();
            SchemaElement se;
            try {
                se = SchemaElement.GetElement(type);
            } catch (Exception e) {
                // skip this type
                continue;
            }
            if (!se.instanceOf(XnatSubjectassessordata.SCHEMA_ELEMENT_NAME) || se.getElementSecurity() == null) {
                // we only want subject assessors that have security settings (aka are configured data types)
                continue;
            }
            typesForVisit.add(type);
        }
        return typesForVisit;
    }

    @JsonInclude
    private class XnatDatatype {
        private String xsitype;
        private String name;
        private String modality;

        public XnatDatatype(){}
        public XnatDatatype(SchemaElement se, String type) {
            xsitype = type;
            modality = ProtocolVisitSubjectHelper.getModalityForXsitype(xsitype);
            name = se.getElementSecurity().getSingular();
        }

        public String getXsitype() {
            return xsitype;
        }

        public void setXsitype(String xsitype) {
            this.xsitype = xsitype;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getModality() {
            return modality;
        }

        public void setModality(String modality) {
            this.modality = modality;
        }
    }
}
