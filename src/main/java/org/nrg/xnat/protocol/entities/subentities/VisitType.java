/*
 * protocols: org.nrg.xnat.protocol.entities.subentities.VisitType
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.protocol.entities.subentities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;
import org.nrg.xnat.protocol.entities.Protocol;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"protocol", "name"}))
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "nrg")
public class VisitType extends AbstractHibernateEntity implements Comparable<VisitType> {
    private Protocol protocol;
    private String description;
    private List<ExpectedExperiment> expectedExperiments = new ArrayList<>();
    private String name;
    private boolean initial = false;
    private boolean terminal = false;
    private int sortOrder;
    private int delta;
    private int deltaDrift;
    private int deltaLow;
    private int deltaHigh;

    public VisitType() {
        Date now = new Date();
        this.setEnabled(true);
        if(this.getCreated() == null){
            this.setCreated(now);
        }
        if(this.getDisabled() == null){
            this.setDisabled(now);
        }
        if(this.getTimestamp() == null){
            this.setTimestamp(now);
        }
    }

    public VisitType(String name, ArrayList<ExpectedExperiment> expectedExperiments, String description, boolean terminal) {
        this();
        this.description = description;
        this.expectedExperiments = expectedExperiments;
        this.name = name;
        this.terminal = terminal;
    }

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "protocol")
    public Protocol getProtocol() {
        return protocol;
    }

    @JsonIgnore
    public void setProtocol(Protocol protocol) {
        this.protocol = protocol;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @OneToMany(mappedBy = "visitType", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public List<ExpectedExperiment> getExpectedExperiments() {
        return this.expectedExperiments;
    }

    public void setExpectedExperiments(List<ExpectedExperiment> expectedExperiments) {
        if(expectedExperiments != null) {
            this.expectedExperiments = expectedExperiments;
            for(ExpectedExperiment ee: expectedExperiments){
                ee.setVisitType(this);
            }
        }
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getTerminal() {
        return terminal;
    }

    public void setTerminal(boolean terminal) {
        this.terminal = terminal;
    }

    public boolean getInitial() {
        return initial;
    }

    public void setInitial(boolean initial) {
        this.initial = initial;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public int getDelta() {
        return delta;
    }

    public void setDelta(int delta) {
        this.delta = delta;
    }

    public int getDeltaDrift() {
        return deltaDrift;
    }

    public void setDeltaDrift(int deltaDrift) {
        this.deltaDrift = deltaDrift;
    }

    public int getDeltaHigh() {
        return deltaHigh;
    }

    public void setDeltaHigh(int deltaHigh) {
        this.deltaHigh = deltaHigh;
    }

    public int getDeltaLow() {
        return deltaLow;
    }

    public void setDeltaLow(int deltaLow) {
        this.deltaLow = deltaLow;
    }

    /**
     * Is there an an expected experiment for this visit type matching the xsitype & subtype
     * @param type the experiment xsitype
     * @param subtype the subtype or null to not filter on subtype
     * @return T/F
     */
    @Transient
    public boolean isExpectedExperiment(String type, @Nullable String subtype) {
        return getExpectedExperiment(type, subtype) != null;
    }

    /**
     * Is there an an expected experiment for this visit type matching the xsitype & subtype
     * @param type the experiment xsitype
     * @param subtype the subtype or null to not filter on subtype
     * @param filterOnSubtype if True, null subtype only returns experiments without subtypes
     * @return T/F
     */
    @Transient
    public boolean isExpectedExperiment(String type, @Nullable String subtype, boolean filterOnSubtype) {
        return getExpectedExperiment(type, subtype, filterOnSubtype) != null;
    }

    /**
     * Return expected experiment for this visit type matching the xsitype & subtype
     * @param type the experiment xsitype
     * @param subtype the subtype or null to skip filtering on subtype
     * @return the expected experiment
     */
    @Transient
    public ExpectedExperiment getExpectedExperiment(String type, @Nullable String subtype) {
        return getExpectedExperiment(type, subtype, false);
    }

    /**
     * Return expected experiment for this visit type matching the xsitype & subtype
     * @param type the experiment xsitype
     * @param subtype the subtype
     * @param filterOnSubtype if True, null subtype only returns experiments without subtypes
     * @return the expected experiment
     */
    @Transient
    public ExpectedExperiment getExpectedExperiment(String type, @Nullable String subtype, boolean filterOnSubtype) {
        for (ExpectedExperiment ee : expectedExperiments) {
            if (ee.getType().equals(type) &&
                    ((subtype == null && !filterOnSubtype) || Objects.equals(subtype, ee.getSubtype()))) {
                return ee;
            }
        }
        return null;
    }

    /**
     * Get all subtypes for datatype
     * @param datatype the datatype
     * @return the subtypes or empty set
     */
    @Transient
    public Set<String> getSubtypesForDatatype(@Nonnull String datatype) {
        Set<String> subtypes = new HashSet<>();
        for (ExpectedExperiment ee : expectedExperiments) {
            if (ee.getType().equals(datatype) && StringUtils.isNotBlank(ee.getSubtype())) {
                subtypes.add(ee.getSubtype());
            }
        }
        return subtypes;
    }

    /**
     * Get all datatypes collected at this visit
     * @return the datatypes or empty set
     */
    @Transient
    public Set<String> getDatatypesForVisit() {
        Set<String> datatypes = new HashSet<>();
        for (ExpectedExperiment ee : expectedExperiments) {
            datatypes.add(ee.getType());
        }
        return datatypes;
    }

    @Transient
    public String validate(List<VisitType> visitTypes) {
        StringBuilder errors = new StringBuilder();
        ArrayList<String> experimentsPairs = new ArrayList<>();
        for (ExpectedExperiment ee : expectedExperiments) {
            if (experimentsPairs.contains(ee.getType() + ee.getSubtype())) {
                errors.append("One or more expected experiments for visit type ").append(name)
                        .append(" cannot be differentiated. Add a 'subtype' to expected experiments that share a type.\n");
            }
            experimentsPairs.add(ee.getType() + ee.getSubtype());
        }
        return errors.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VisitType)) return false;
        VisitType visitType = (VisitType) o;
        if (initial != visitType.initial) return false;
        if (terminal != visitType.terminal) return false;
        if (sortOrder != visitType.sortOrder) return false;
        if (delta != visitType.delta) return false;
        if (description != null ? !description.equals(visitType.description) : visitType.description != null) return false;
        if (expectedExperiments != null && visitType.expectedExperiments != null) {
            if(expectedExperiments.size() != visitType.expectedExperiments.size()) return false;
            for (int i = 0; i < expectedExperiments.size(); i++) {
                Object exp = expectedExperiments.get(i);
                Object vtExp = visitType.expectedExperiments.get(i);
                if(!exp.equals(vtExp)) {
                    return false;
                }
            }
        }
        if (name != null ? !name.equals(visitType.name) : visitType.name != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) this.getId();
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (expectedExperiments != null ? expectedExperiments.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (initial ? 1 : 0);
        result = 31 * result + (terminal ? 1 : 0);
        result = 31 * result + delta;
        result = 31 * result + deltaDrift;
        return result;
    }

    @Override
    public int compareTo(VisitType that) {
        return this.name.compareToIgnoreCase(that.name);
    }
}
