package org.nrg.xnat.protocol.services.impl;

import org.apache.commons.lang3.StringUtils;
import org.nrg.action.ClientException;
import org.nrg.xdat.om.XnatPvisitdata;
import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.security.ElementSecurity;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.protocol.entities.Protocol;
import org.nrg.xnat.protocol.entities.subentities.VisitType;
import org.nrg.xnat.protocol.services.ProjectProtocolService;
import org.nrg.xnat.protocol.services.VisitTypeService;
import org.nrg.xnat.protocol.util.NamingTemplateFields;
import org.nrg.xnat.protocol.util.ProtocolVisitSubjectHelper;
import org.nrg.xnat.services.archive.SubjectAssessorLabelingService;
import org.restlet.data.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

@Service
public class ProtocolLabeler implements SubjectAssessorLabelingService {
    private final VisitTypeService visitTypeService;
    private final ProjectProtocolService projectProtocolService;

    @Autowired
    public ProtocolLabeler(VisitTypeService visitTypeService,
                           ProjectProtocolService projectProtocolService) {
        this.visitTypeService = visitTypeService;
        this.projectProtocolService = projectProtocolService;
    }

    @Override
    public String determineLabel(XnatSubjectassessordata sad, Map<String, Object> params, UserI user) {
        try {
            return generateLabel(sad.getProject(), sad.getSubjectId(), sad.getVisit(), sad.getProtocol(), sad.getXSIType(), null,
                    sad.getDate(), "yyyy-MM-dd", user);
        } catch (ClientException e) {
            // The details of this will come through in validation
            return null;
        }
    }

    @Nullable
    public String generateLabel(@Nullable String projectId,
                                @Nullable String subjectId,
                                @Nullable String visitId,
                                @Nullable String subtype,
                                @Nullable String dataType,
                                @Nullable String modality,
                                @Nullable Object date,
                                @Nullable String dateFormat,
                                @Nonnull UserI user) throws ClientException {

        Protocol protocol = null;
        String visitName = null;
        if (StringUtils.isNotBlank(visitId)) {
            XnatPvisitdata pvisitdata = XnatPvisitdata.getXnatPvisitdatasById(visitId, user, false);
            if (pvisitdata == null) {
                throw new ClientException(Status.CLIENT_ERROR_BAD_REQUEST, "Visit is invalid");
            }

            String visitTypeName = pvisitdata.getVisitType();
            if (visitTypeName != null) {
                VisitType vt = visitTypeService.getVisitType(Long.parseLong(pvisitdata.getProtocolid()), visitTypeName, projectId);
                if (vt == null) {
                    throw new ClientException(Status.CLIENT_ERROR_BAD_REQUEST, "Visit does not have a valid type");
                }
                protocol = vt.getProtocol();
                visitName = vt.getName();
            } else {
                visitName = pvisitdata.getVisitName();
            }

            if (projectId == null) {
                projectId = pvisitdata.getProject();
            }
            if (subjectId == null) {
                subjectId = pvisitdata.getSubjectId();
            }
        }

        if (protocol == null && StringUtils.isNotBlank(projectId)) {
            protocol = projectProtocolService.getProtocolForProject(projectId, user);
        }

        if (protocol == null || StringUtils.isBlank(protocol.getExperimentNamingTemplate())) {
            return null;
        }

        XnatSubjectdata subject;
        String subjectLabel = null;
        if (StringUtils.isNotBlank(subjectId) && (subject =
                XnatSubjectdata.getXnatSubjectdatasById(subjectId, user, false)) != null) {
            subjectLabel = subject.getLabel();
        }

        String dateStr = null;
        if (date != null && (!(date instanceof String) || StringUtils.isNotBlank((String) date))) {
            // from edit page, date comes in as "MM/dd/yyyy", from XNAT db: "yyyy-MM-dd"
            dateStr = new SimpleDateFormat("yyyyMMdd")
                    .format(ProtocolVisitSubjectHelper.determineDate(date, dateFormat));
        }

        Map<NamingTemplateFields, String> params = new HashMap<>();
        params.put(NamingTemplateFields.PROJECT, projectId);
        params.put(NamingTemplateFields.SUBJECT, subjectLabel);
        params.put(NamingTemplateFields.VISIT, visitName);
        params.put(NamingTemplateFields.DATE, dateStr);
        String type = StringUtils.defaultIfBlank(modality, generateDataTypeStr(dataType));
        params.put(NamingTemplateFields.DATATYPE, StringUtils.isNotBlank(subtype) ? type + subtype : type);
        return protocol.generateExperimentLabel(params);
    }

    @Nullable
    private String generateDataTypeStr(@Nullable String dataType) {
        if (StringUtils.isBlank(dataType)) {
            return null;
        }
        return ProtocolVisitSubjectHelper.getModalityForXsitype(dataType);
    }
}
