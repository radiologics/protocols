package org.nrg.xnat.protocol.services;

import org.nrg.xnat.protocol.entities.subentities.VisitType;

public interface VisitTypeService {
    VisitType getVisitType(long protocolLineageId, String visitType, String projectId);
}
