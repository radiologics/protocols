package org.nrg.xnat.protocol.services.impl;

import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.protocol.util.ProtocolVisitSubjectHelper;
import org.nrg.xnat.services.archive.SubjectAssessorValidationService;
import org.restlet.data.Status;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ProtocolValidation implements SubjectAssessorValidationService {

    @Override
    public void validate(XnatSubjectassessordata sad, Map<String, Object> params, UserI user)
            throws ServerException, ClientException {

        String visitId = sad.getVisit();
        if (visitId == null) {
            // Don't do this validation if we're not uploading to a visit
            return;
        }

        // Confirm datatype
        String dataType = sad.getXSIType();
        Object dataTypeExpected = params.get("datatype");
        if (dataTypeExpected != null && !dataType.equals(dataTypeExpected)) {
            throw new ServerException(Status.CLIENT_ERROR_BAD_REQUEST, "Data type for item (" + dataType
                    + ") doesn't match expected (" + dataTypeExpected + ")");
        }

        String subtype = sad.getProtocol();
        ProtocolVisitSubjectHelper.validateExperimentVisit(dataType, visitId,
                sad.getDate(), sad.getSubjectId(), subtype, user);
    }
}
