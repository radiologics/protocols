package org.nrg.xnat.protocol.services.impl.hibernate;

import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xnat.protocol.daos.VisitTypeDAO;
import org.nrg.xnat.protocol.entities.subentities.VisitType;
import org.nrg.xnat.protocol.services.VisitTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nullable;

@Service
public class HibernateVisitTypeService  extends AbstractHibernateEntityService<VisitType, VisitTypeDAO> implements VisitTypeService {
    @Override
    @Transactional
    @Nullable
    public VisitType getVisitType(long protocolLineageId, String visitType, String projectId) {
        return getDao().findByNameAndProtocolLineage(protocolLineageId, visitType, projectId);
    }
}
