/*
 * protocols: org.nrg.xnat.protocol.util.ProtocolVisitSubjectHelper
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.protocol.util;

import org.apache.commons.lang3.StringUtils;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatPvisitdata;
import org.nrg.xdat.security.ElementSecurity;
import org.nrg.xdat.security.helpers.Roles;
import org.nrg.xdat.security.helpers.UserHelper;
import org.nrg.xft.XFTTable;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xft.search.TableSearch;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.protocol.entities.Protocol;
import org.nrg.xnat.protocol.entities.subentities.ExpectedExperiment;
import org.nrg.xnat.protocol.entities.subentities.VisitType;
import org.nrg.xnat.protocol.services.ProjectProtocolService;
import org.nrg.xnat.protocol.services.ProtocolExceptionService;
import org.nrg.xnat.protocol.services.ProtocolService;
import org.nrg.xnat.protocol.services.VisitTypeService;
import org.restlet.data.Status;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ProtocolVisitSubjectHelper {
    //protected Map<XnatPvisitdata, List<ExperimentAssessorContainer>> visits = null;

    public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ProtocolVisitSubjectHelper.class);
    private static ProtocolService protocolService = null;
    private static ProtocolExceptionService protocolExceptionService = null;
    private static ProjectProtocolService projectProtocolService = null;
    private static VisitTypeService visitTypeService = null;

    public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";

    public ProtocolVisitSubjectHelper() {

    }

    public Protocol getProtocol(String projectID, UserI user) {
        if (projectID == null) {
            return null;
        }

        final XnatProjectdata p = XnatProjectdata.getXnatProjectdatasById(projectID, user, false);
        if (p == null) return null;

        Protocol protocol = getProjectProtocolService().getProtocolForProject(projectID, user);
        if (protocol == null) {
            // probably just doesn't have a protocol
            logger.debug("no protocol found for this project: " + projectID);
            return null;
        }
        String validateString = protocol.validate();
        if (validateString != null) {
            logger.error("the protocol is invalid: " + validateString);
            return null;
        }
        return protocol;
    }

    public boolean visitHasExperimentOrException(String visitId, String xsiType, String subtype, UserI user, boolean exceptionsAllowed) {
        boolean hasExperiment = visitHasExperiment(visitId, xsiType, subtype);
        boolean hasException = getProtocolExceptionService().findExceptionForVisitAndType(visitId, xsiType, subtype) != null;
        return hasExperiment || (exceptionsAllowed && hasException);
    }

    public static boolean visitHasExperiment(String visitId, String xsiType, String subtype) {
        try {
            XFTTable table = TableSearch.Execute("SELECT * " +
                    "FROM xnat_experimentData ex " +
                    "INNER JOIN xdat_meta_element me ON ex.extension=me.xdat_meta_element_id " +
                    "WHERE ex.visit = '" + visitId + "' " +
                    "AND me.element_name = '" + xsiType +
                    (subtype != null ? "' AND ex.protocol = '" + subtype.replace("\\", "\\\\") + "';" : "' AND ex.protocol IS NULL;"),
                    null, null);
            table.resetRowCursor();


            return table.hasMoreRows();
        }
        catch (java.lang.Exception e) {
            logger.error("", e);
        }

        return false;
    }

    public static boolean hasOpenVisits(String projectId, String subjectId, UserI user) {
        try {
            XFTTable table = TableSearch.Execute("SELECT * " +
                    "FROM xnat_experimentData ex " +
                    "INNER JOIN xnat_pvisitdata vd ON ex.id=vd.id " +
                    "WHERE ex.project = '" + projectId + "' " +
                    "AND vd.subject_id = '" + subjectId + "' " +
                    "AND (vd.closed = 0 OR vd.closed IS NULL);",
                    null, null);
            table.resetRowCursor();
            return table.hasMoreRows();
        }
        catch (java.lang.Exception e) {
            logger.error("", e);
        }

        return false;
    }

    public static boolean hasTerminalVisit(String projectId, String subjectId, Date date, UserI user) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");

            XFTTable table = TableSearch.Execute("SELECT * " +
                    "FROM xnat_experimentData ex " +
                    "INNER JOIN xnat_pvisitdata vd ON ex.id=vd.id " +
                    "WHERE ex.project = '" + projectId + "' " +
                    (date != null ? ("AND ex.date < '" + sdf.format(date) + "' ") : "") +
                    "AND vd.subject_id = '" + subjectId + "' " +
                    "AND vd.terminal = 1;",
                    null, null);
            table.resetRowCursor();
            return table.hasMoreRows();
        }
        catch (java.lang.Exception e) {
            logger.error("", e);
        }

        return false;
    }

    public static XnatPvisitdata getVisitByVisitName(String projectId, String subjectId, String visitName, UserI user) {
        try {
            XFTTable table = TableSearch.Execute("SELECT vd.id AS visit_id " +
                    "FROM xnat_experimentData ex " +
                    "INNER JOIN xnat_pvisitdata vd ON ex.id=vd.id " +
                    "WHERE ex.project = '" + projectId + "' " +
                    "AND vd.subject_id = '" + subjectId + "' " +
                    "AND vd.visit_name = '" + visitName.replace("\\", "\\\\") + "';",
                    null, null);
            table.resetRowCursor();
            if (table.hasMoreRows()) {
                Hashtable row = table.nextRowHash();
                final Object visit_id = row.get("visit_id");
                XnatPvisitdata visit = new XnatPvisitdata(ItemSearch.GetItem("xnat:pVisitData/id", visit_id, null, true));
                return visit;
            }
        }
        catch (java.lang.Exception e) {
            logger.error("", e);
        }
        return null;
    }

    public Collection<String> getSubtypesForVisit(String visitId, UserI user) {
        Set<String> subtypes = new HashSet<String>();
        try {
            XnatPvisitdata visit = new XnatPvisitdata(ItemSearch.GetItem("xnat:pVisitData/id", visitId, null, true));
            Protocol protocol = getProtocol(visit.getProject(), user);
            if (protocol != null && visit != null) {
                String visitTypeName = visit.getVisitType();
                for (VisitType visitType : protocol.getVisitTypes()) {
                    if (visitType.getName().equals(visitTypeName)) {
                        for (ExpectedExperiment expectedExperiment : visitType.getExpectedExperiments()) {
                            if (!StringUtils.isEmpty(expectedExperiment.getSubtype())) {
                                subtypes.add(expectedExperiment.getSubtype());
                            }
                        }
                    }
                }
            }
        } catch (java.lang.Exception e) {
            logger.error("", e);
        }
        return subtypes;
    }

    /**
     * Validate experiment assignment to visit
     * @param dataType the datatype
     * @param visitId the pvisit id
     * @param exptDateObj the exp date
     * @param subjectId the subject
     * @param subtype the subtype
     * @param user the user
     * @throws ClientException for warnings that can be overridden
     * @throws ServerException for errors that make the data invalid
     */
    public static void validateExperimentVisit(@Nonnull String dataType,
                                               @Nonnull String visitId,
                                               @Nonnull Object exptDateObj,
                                               @Nonnull String subjectId,
                                               @Nullable String subtype,
                                               @Nonnull UserI user) throws ClientException, ServerException {

        // Confirm it's an open visit for this subject
        XnatPvisitdata pvisitdata = XnatPvisitdata.getXnatPvisitdatasById(visitId, user, false);
        if (pvisitdata == null) {
            throw new ServerException(Status.CLIENT_ERROR_BAD_REQUEST, "Visit is invalid");
        }
        Boolean closed = pvisitdata.getClosed();
        if (closed != null && closed) {
            throw new ServerException(Status.CLIENT_ERROR_BAD_REQUEST, "Visit is closed");
        }
        if (!pvisitdata.getSubjectId().equals(subjectId)) {
            throw new ServerException(Status.CLIENT_ERROR_BAD_REQUEST,
                    "Visit does not correspond to subject: " + subjectId);
        }

        // Confirm date within delta
        Date exptDate = determineDate(exptDateObj);

        String projectId     = pvisitdata.getProject();
        String visitTypeName = pvisitdata.getVisitType();
        Date   visitDate     = (Date) pvisitdata.getDate();
        boolean adhoc = visitTypeName == null;
        if (adhoc) {
            // Ad hc visit, no further validation
            if (!exptDate.equals(visitDate)) {
                throw new ClientException(Status.CLIENT_ERROR_BAD_REQUEST, "Experiment on " + exptDate
                        + " is out of range for visit dated " + visitDate);
            }
            return;
        }

        VisitType vt = getVisitTypeByNameFromPVisitAndProject(pvisitdata, visitTypeName, projectId);
        Calendar visitDateMin = Calendar.getInstance();
        Calendar visitDateMax = Calendar.getInstance();
        visitDateMin.setTime(visitDate);
        visitDateMax.setTime(visitDate);
        visitDateMin.add(Calendar.DAY_OF_MONTH, vt.getDeltaLow() * -1);
        visitDateMax.add(Calendar.DAY_OF_MONTH, vt.getDeltaHigh());
        if (exptDate.before(visitDateMin.getTime()) || exptDate.after(visitDateMax.getTime())) {
            throw new ClientException(Status.CLIENT_ERROR_BAD_REQUEST, "Experiment on " + exptDate
                    + " is out of range for visit dated " + visitDate);
        }

        // Validate subtype
        if (StringUtils.isNotBlank(subtype) && !subtype.equals("NULL")) {
            boolean valid = false;
            for (ExpectedExperiment expectedExperiment : vt.getExpectedExperiments()) {
                if (subtype.equals(expectedExperiment.getSubtype()) && dataType.equals(expectedExperiment.getType())) {
                    valid = true;
                    break;
                }
            }
            if (!valid) {
                throw new ServerException(Status.CLIENT_ERROR_BAD_REQUEST, "Subtype " + subtype
                        + " is not valid for visit " + visitTypeName + " datatype " + dataType);
            }
        } else {
            subtype = null; // set NULL to null
        }

        // Validate datatype
        Protocol protocol = vt.getProtocol();
        boolean unexpectedOk = protocol.getAllowUnexpectedExperiments() &&
                (UserHelper.getUserHelperService(user).isOwner(projectId) || !Roles.isSiteAdmin(user));
        if (!unexpectedOk &&
                ProtocolVisitSubjectHelper.visitHasExperiment(visitId, dataType, subtype)) {
            throw new ServerException(Status.CLIENT_ERROR_BAD_REQUEST, "Subject " + subjectId
                    + " already has an experiment for visit " + visitTypeName + " datatype " + dataType +
                    (subtype != null ? " subtype " + subtype : "") + ". To upload, the protocol must allow " +
                    "unexpected visits and you must be a project owner or admin.");
        }
    }

    @Nonnull
    public static Date determineDate(Object exptDateObj) throws ClientException {
        return determineDate(exptDateObj, null);
    }

    @Nonnull
    public static Date determineDate(Object exptDateObj, @Nullable String datePattern) throws ClientException {
        Date exptDate;
        if (exptDateObj instanceof Date) {
            exptDate = (Date) exptDateObj;
        } else if (exptDateObj instanceof String) {
            datePattern = StringUtils.defaultIfBlank(datePattern, DEFAULT_DATE_PATTERN);
            try {
                exptDate = new SimpleDateFormat(datePattern).parse((String) exptDateObj);
            } catch (ParseException e) {
                throw new ClientException(Status.CLIENT_ERROR_BAD_REQUEST, "Experiment does not have a valid date");
            }
        } else {
            throw new ClientException(Status.CLIENT_ERROR_BAD_REQUEST, "Experiment does not have a valid date");
        }
        return exptDate;
    }

    @Nonnull
    public static String getModalityForXsitype(String xsiType) {
        String modality = null;
        try {
            modality = ElementSecurity.GetElementSecurity(xsiType).getCode();
            if (StringUtils.isNotBlank(modality)) {
                // PET modality is PT, but often its code in XNAT is PET
                modality = modality.replaceAll("PET", "PT");
            }
        } catch (Exception e) {
            // Ignore
        }
        return StringUtils.defaultIfBlank(modality,
                xsiType.replaceAll("^.*:([a-z]*).*$", "$1").toUpperCase());
    }

    private static VisitType getVisitTypeByNameFromPVisitAndProject(XnatPvisitdata pvisitdata,
                                                                    String visitTypeName,
                                                                    String projectId)
            throws ServerException {
        VisitTypeService vts = getVisitTypeService();
        if (vts == null) {
            throw new ServerException(Status.SERVER_ERROR_INTERNAL, "Unable to find VisitTypeService bean");
        }
        VisitType vt = vts.getVisitType(Long.parseLong(pvisitdata.getProtocolid()), visitTypeName, projectId);
        if (vt == null) {
            throw new ServerException(Status.CLIENT_ERROR_BAD_REQUEST, "Visit does not have a valid type");
        }
        return vt;
    }

    private static ProtocolService getProtocolService() {
        if (protocolService == null) {
            protocolService = XDAT.getContextService().getBean(ProtocolService.class);
        }
        return protocolService;
    }

    private static ProtocolExceptionService getProtocolExceptionService() {
        if (protocolExceptionService == null) {
            protocolExceptionService = XDAT.getContextService().getBean(ProtocolExceptionService.class);
        }
        return protocolExceptionService;
    }

    private static ProjectProtocolService getProjectProtocolService() {
        if (projectProtocolService == null) {
            projectProtocolService = XDAT.getContextService().getBean(ProjectProtocolService.class);
        }
        return projectProtocolService;
    }

    private static VisitTypeService getVisitTypeService() {
        if (visitTypeService == null) {
            visitTypeService = XDAT.getContextService().getBean(VisitTypeService.class);
        }
        return visitTypeService;
    }
}
