package org.nrg.xnat.protocol.util;

public enum NamingTemplateFields {
    PROJECT,
    SUBJECT,
    VISIT,
    DATATYPE,
    DATE
}