/*
 * protocols: org.nrg.xnat.protocol.daos.ProtocolDAO
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.protocol.daos;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.xnat.protocol.entities.Protocol;
import org.nrg.xnat.protocol.entities.subentities.VisitType;
import org.springframework.stereotype.Repository;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unchecked")
@Repository
public class VisitTypeDAO extends AbstractHibernateDAO<VisitType> {
    @Nullable
    public VisitType findByNameAndProtocolLineage(long protocolLineageId, String name, String projectId) {
        Criteria criteria = getCriteriaForType();
        criteria = criteria.createAlias("protocol", "protocol");
        criteria = criteria.createAlias("protocol.projectProtocols", "projectProtocol", JoinType.LEFT_OUTER_JOIN);
        criteria.add(Restrictions.eq("protocol.protocolLineage.id", protocolLineageId));
        criteria.add(Restrictions.eq("projectProtocol.projectId", projectId));
        criteria.add(Restrictions.eq("name", name));
        criteria.addOrder(Order.desc("projectProtocol.created"));
        criteria.setMaxResults(1);
        return (VisitType) criteria.uniqueResult();
    }
}
