/*
 * protocols: org.nrg.xnat.restlet.extensions.ProjSubVisitList
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.restlet.extensions;

import org.apache.log4j.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xnat.helpers.xmlpath.XMLPathShortcuts;
import org.nrg.xnat.protocol.entities.subentities.VisitType;
import org.nrg.xnat.protocol.util.SubjectVisitInfo;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@XnatRestlet({"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/visits"})
public class ProjSubVisitList extends SecureResource {

    static Logger logger = Logger.getLogger(ProjSubVisitList.class);

    XnatProjectdata proj = null;
    XnatSubjectdata subject = null;
    String xsiType = null;
    String subtype = null;
    String sessionId = null;
    private final ObjectMapper mapper = new ObjectMapper();

    public ProjSubVisitList(Context context, Request request, Response response) {
        super(context, request, response);

        String pID = getUrlEncodedParameter(request, "PROJECT_ID");
        if(pID != null){
            proj = XnatProjectdata.getProjectByIDorAlias(pID, getUser(), false);
        }

        if (proj == null) {
            response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
            getResponse().setEntity("Unable to identify project " + pID + ".", MediaType.TEXT_PLAIN);
            return;
        }

        String subID = getUrlEncodedParameter(request, "SUBJECT_ID");
        if(subID!=null){
            subject = XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(), subID, getUser(), false);
            if(subject==null){
                subject = XnatSubjectdata.getXnatSubjectdatasById(subID, getUser(), false);
                if (subject != null && (proj != null && !subject.hasProject(proj.getId()))) {
                    subject = null;
                }
            }
        }
        if(subject == null){
            response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
            getResponse().setEntity("Unable to identify subject " + subID + ".", MediaType.TEXT_PLAIN);
            return;
        }

        Map<String, String> queryVaribleMap = getDecodedQueryVariableMap();
        sessionId = queryVaribleMap.get("sessionID");
        xsiType   = queryVaribleMap.get("type");
        subtype   = queryVaribleMap.get("subtype");

        this.getVariants().add(new Variant(MediaType.APPLICATION_JSON));
        this.getVariants().add(new Variant(MediaType.TEXT_HTML));
        this.getVariants().add(new Variant(MediaType.TEXT_XML));


        this.fieldMapping.putAll(XMLPathShortcuts.getInstance().getShortcuts(XMLPathShortcuts.VISIT_DATA,true));
    }

    @Override
    public Representation represent(Variant variant) throws ResourceException {

        try {
            SubjectVisitInfo subjectVisitInfo = new SubjectVisitInfo(subject, proj.getId(), getUser());
            String response;
            if (this.isQueryVariableTrue("initial")) {
                List<String> initialVisits = new ArrayList<>();
                List<VisitType> visitTypes = subjectVisitInfo.getProtocol().getVisitTypes();
                for (VisitType visitType : visitTypes) {
                    if (visitType.getInitial() && (xsiType == null || visitType.isExpectedExperiment(xsiType, subtype, false))) {
                        initialVisits.add(visitType.getName());
                    }
                }
                response = mapper.writeValueAsString(initialVisits.toArray());
            }
            else if (this.isQueryVariableTrue("open")) {
                List<SubjectVisitInfo.VisitInfo> openVisits = new ArrayList<>();
                for (SubjectVisitInfo.VisitInfo visitInfo : subjectVisitInfo.getVisits()) {
                    if (!visitInfo.getClosed() && (xsiType == null || visitInfo.getAdHoc() ||
                            visitInfo.isExpectedExperiment(xsiType, subtype, false))) {
                        openVisits.add(visitInfo);
                    }
                }
                response = mapper.writeValueAsString(openVisits.toArray());
            }
            else if (sessionId != null) {
                XnatExperimentdata session = XnatExperimentdata.getXnatExperimentdatasById(sessionId, getUser(), false);
                if (session != null) {
                    String type = session.getXSIType();
                    if (subtype == null) subtype = session.getProtocol();
                    response = mapper.writeValueAsString(subjectVisitInfo.getValidVisitsForExperiment(type, subtype));
                } else {
                    throw new Exception("No session matching " + sessionId);
                }
            }
            else {
                response = mapper.writeValueAsString(subjectVisitInfo.getVisits());
            }

            return new StringRepresentation(response, MediaType.APPLICATION_JSON);
        } catch (Exception e) {
            logger.error("", e);
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, "Error retrieving visit list.");
        }
    }
}
