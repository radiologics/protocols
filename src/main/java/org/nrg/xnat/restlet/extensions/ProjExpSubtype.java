/*
 * protocols: org.nrg.xnat.restlet.extensions.ProjExpSubtype
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.restlet.extensions;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatPvisitdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.om.base.auto.AutoXnatPvisitdata;
import org.nrg.xdat.security.helpers.Roles;
import org.nrg.xdat.security.helpers.UserHelper;
import org.nrg.xnat.protocol.entities.Protocol;
import org.nrg.xnat.protocol.entities.subentities.VisitType;
import org.nrg.xnat.protocol.util.ProtocolVisitSubjectHelper;
import org.nrg.xnat.restlet.XnatRestlet;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

@XnatRestlet({"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXPERIMENT_ID}/subtype", "/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXPERIMENT_ID}/subtype/{SUBTYPE}"})
public class ProjExpSubtype extends AbstractProtocolResource {
    private static final Log _log = LogFactory.getLog(ProjExpSubtype.class);
    XnatProjectdata project = null;
    XnatSubjectdata subject = null;
    XnatExperimentdata experiment = null;
    XnatPvisitdata visit = null;
    String subtype = null;

    public ProjExpSubtype(Context context, Request request, Response response) throws UnsupportedEncodingException {
        super(context, request, response);

        //we need to grab the project, subject, experiment and visit.
        String projectId = (String) getParameter(request, "PROJECT_ID");
        if (projectId != null) {
            project = XnatProjectdata.getProjectByIDorAlias(projectId, getUser(), false);
        }
        if (project == null) {
            response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
            response.setEntity("Unable to identify project " + projectId, MediaType.TEXT_PLAIN);
            return;
        }

        String subjectId = (String) getParameter(request, "SUBJECT_ID");
        if (subjectId != null) {
            subject = XnatSubjectdata.GetSubjectByProjectIdentifier(project.getId(), subjectId, getUser(), false);
        }
        if (subject == null) {
            subject = XnatSubjectdata.getXnatSubjectdatasById(subjectId, getUser(), false);
            if (subject != null && (project != null && !subject.hasProject(project.getId()))) {
                subject = null;
            }
        }
        if (subject == null) {
            response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
            response.setEntity("Unable to identify subject " + subjectId, MediaType.TEXT_PLAIN);
        }

        String experimentId = (String) getParameter(request, "EXPERIMENT_ID");
        if (experimentId != null) {
            experiment = XnatExperimentdata.getXnatExperimentdatasById(experimentId, getUser(), completeDocument);
            if (experiment != null && (project != null && !experiment.hasProject(project.getId()))) {
                response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
                response.setEntity("Experiment " + experimentId + " not associated with project " + projectId + ".", MediaType.TEXT_PLAIN);
                return;
            }
        }
        if (experiment == null) {
            response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
            response.setEntity("Unable to identify experiment " + experimentId + ".", MediaType.TEXT_PLAIN);
            return;
        }

        String visitId = experiment.getVisit();
        if (visitId == null ||
                (visit = AutoXnatPvisitdata.getXnatPvisitdatasById(visitId, getUser(), false)) == null) {
            response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            response.setEntity("Experiment " + experimentId + " not associated with a visit. " +
                    "You must set a visit first, then add a subtype", MediaType.TEXT_PLAIN);
            return;
        }

        if (visit.getClosed() != null && visit.getClosed()) {
            response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
            response.setEntity("Experiment " + experimentId + " is associated with a closed visit and " +
                    "cannot be modified.", MediaType.TEXT_PLAIN);
            return;
        }

        subtype = request.getAttributes().containsKey("SUBTYPE") ? URLDecoder.decode((String) getParameter(request, "SUBTYPE"), "UTF-8") : null;
        // expanded to allow subtypes to be entered as query variables to get around a Tomcat problem with encoded slashes in parameters
        if (subtype == null && hasQueryVariable("subtype")) {
            subtype = this.getQueryVariable("subtype").replace("&amp;", "&"); // subtype will have been double encoded
        }

        if (StringUtils.isBlank(subtype) || subtype.equals("undefined")) {
            subtype = null;
        }
    }


    @Override
    public boolean allowDelete() {
        try {
            return experiment.canEdit(getUser());
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void handleDelete() {
        experiment.setProtocol("NULL");
        try {
            experiment.save(getUser(), true, false, null);
        } catch (Exception e) {
            String msg = "Unable to remove subtype from experiment " + experiment.getId();
            _log.error(msg, e);
            this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
            getResponse().setEntity(msg + ": " + e.getMessage(), MediaType.TEXT_PLAIN);
            return;
        }
        getResponse().setStatus(Status.SUCCESS_OK);
    }


    @Override
    public boolean allowPut() {
        try {
            return experiment.canEdit(getUser());
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void handlePut() {
        String projectId = project.getId();

        VisitType vt = getVisitTypeService().getVisitType(Long.parseLong(visit.getProtocolid()), visit.getVisitType(), projectId);
        if (vt == null) {
            getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            getResponse().setEntity("Visit " + visit + " does not have a visit type.", MediaType.TEXT_PLAIN);
            return;
        }

        Protocol protocol = vt.getProtocol();
        if (protocol == null) {
            getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            getResponse().setEntity("Project " + projectId + " does not have a protocol associated with it.", MediaType.TEXT_PLAIN);
            return;
        }

        // If unexpected experiments are not allowed, check if we have them
        boolean unexpectedOk = protocol.getAllowUnexpectedExperiments() &&
                (UserHelper.getUserHelperService(getUser()).isOwner(projectId) || !Roles.isSiteAdmin(getUser()));
        if (!unexpectedOk) {
            String unexpectedMsg = null;
            String dataType = experiment.getXSIType();
            if (!vt.isExpectedExperiment(dataType, subtype)) {
                unexpectedMsg = "This protocol does not expect experiments of type " + dataType +
                        " with subtype " + subtype + " at visit " + vt.getName();
            } else if (ProtocolVisitSubjectHelper.visitHasExperiment(visit.getId(), dataType, subtype)) {
                unexpectedMsg = "An experiment of type " + dataType + " with subtype " + subtype +
                        " already exists for visit " + vt.getName();
            }

            if (unexpectedMsg != null) {
                if (protocol.getAllowUnexpectedExperiments()) {
                    getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
                    getResponse().setEntity(unexpectedMsg + ". Only a project owner or site admin can assign " +
                            "this subtype.", MediaType.TEXT_PLAIN);
                    return;
                } else {
                    getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
                    getResponse().setEntity(unexpectedMsg + " and unexpected experiments are prohibited.",
                            MediaType.TEXT_PLAIN);
                    return;
                }
            }
        }

        if (subtype == null) {
            this.handleDelete();
        } else {
            experiment.setProtocol(subtype);
            try {
                experiment.save(getUser(), true, false, null);
            } catch (Exception e) {
                String msg = "Unable to set subtype for experiment " + experiment.getId();
                _log.error(msg, e);
                this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
                getResponse().setEntity(msg + ": " + e.getMessage(), MediaType.TEXT_PLAIN);
                return;
            }
            getResponse().setStatus(Status.SUCCESS_OK);
        }
    }
}
