/*
 * protocols: org.nrg.xnat.restlet.extensions.ProjExpVisit
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.restlet.extensions;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.model.XnatExperimentdataShareI;
import org.nrg.xdat.om.*;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.protocol.entities.Protocol;
import org.nrg.xnat.protocol.util.ProtocolVisitSubjectHelper;
import org.nrg.xnat.restlet.XnatRestlet;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Variant;

import javax.annotation.Nullable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

@XnatRestlet({"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXPERIMENT_ID}/visit/{VISIT_ID_OR_NAME}"})
public class ProjExpVisit extends AbstractProtocolResource {
    private static final Log _log = LogFactory.getLog(ProjExpVisit.class);
    XnatProjectdata project = null;
    XnatSubjectdata subject = null;
    XnatExperimentdata experiment = null;
    XnatExperimentdataShareI sharedExpt = null;
    XnatPvisitdata visit = null;
    Protocol protocol;
    String subtype = null;

    public ProjExpVisit(Context context, Request request, Response response) throws UnsupportedEncodingException {

        super(context, request, response);

        //we need to grab the project, subject, experiment and visit.
        String projectId = (String) getParameter(request, "PROJECT_ID");
        if (projectId != null) {
            project = XnatProjectdata.getProjectByIDorAlias(projectId, getUser(), false);
        }
        if (project == null) {
            response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
            response.setEntity("Unable to identify project " + projectId, MediaType.TEXT_PLAIN);
            return;
        }

        protocol = getProjectProtocolService().getProtocolForProject(projectId, getUser());
        if (protocol == null) {
            response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            response.setEntity("Project " + projectId + " does not have a protocol associated with it.", MediaType.TEXT_PLAIN);
            return;
        }

        String subjectId = (String) getParameter(request, "SUBJECT_ID");
        if (subjectId != null) {
            subject = XnatSubjectdata.GetSubjectByProjectIdentifier(project.getId(), subjectId, getUser(), false);
        }
        if (subject == null) {
            subject = XnatSubjectdata.getXnatSubjectdatasById(subjectId, getUser(), false);
            if (subject != null && (project != null && !subject.hasProject(project.getId()))) {
                subject = null;
            }
        }
        if (subject == null) {
            response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
            response.setEntity("Unable to identify subject " + subjectId, MediaType.TEXT_PLAIN);
        }

        String experimentId = (String) getParameter(request, "EXPERIMENT_ID");
        if (experimentId != null) {
            experiment = XnatExperimentdata.getXnatExperimentdatasById(experimentId, getUser(), completeDocument);
            if (experiment != null && (project != null && !experiment.hasProject(project.getId()))) {
                response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
                response.setEntity("Experiment " + experimentId + " not associated with project " + projectId + ".", MediaType.TEXT_PLAIN);
                return;
            }
        }
        if (experiment == null) {
            response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
            response.setEntity("Unable to identify experiment " + experimentId + ".", MediaType.TEXT_PLAIN);
            return;
        }

        String visitIdOrName = URLDecoder.decode((String) getParameter(request, "VISIT_ID_OR_NAME"), "UTF-8");
        if (visitIdOrName != null) {
            visit = XnatPvisitdata.getXnatPvisitdatasById(visitIdOrName, getUser(), completeDocument);
            //visits aren't really associated with projects in any way so we don't have to test that here.
        }
        // if no visit was found by id, we check to see whether we were passed the visit name instead
        if (visit == null) {
            CriteriaCollection cc = new CriteriaCollection("AND");
            cc.addClause("xnat:pVisitData/visit_name", visitIdOrName);
            cc.addClause("xnat:pVisitData/subject_id", subjectId);
            cc.addClause("xnat:experimentData/project", projectId);

            List<XnatPvisitdata> list = XnatPvisitdata.getXnatPvisitdatasByField(cc, getUser(), false);
            if (list.size() > 0) {
                visit = list.get(0);
            }
        }
        if (visit == null) {
            response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
            response.setEntity("Unable to identify visit " + visitIdOrName + ".", MediaType.TEXT_PLAIN);
            return;
        }

        String subtypeV = getQueryVariable("subtype");
        if (subtypeV != null && !subtypeV.equals("undefined")) {
            subtype = URLDecoder.decode(subtypeV, "UTF-8");
        }
        subtype = StringUtils.defaultIfBlank(subtype, null);

        if (!project.getId().equals(experiment.getProject())) {
            for (XnatExperimentdataShareI pp : experiment.getSharing_share()) {
                if (pp.getProject().equals(project.getId())) {
                    sharedExpt = pp;
                    break;
                }
            }
            if (sharedExpt == null) {
                response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
                response.setEntity(experiment.getId() + " not shared into project " + project.getId(),
                        MediaType.TEXT_PLAIN);
                return;
            }
        }

        this.getVariants().add(new Variant(MediaType.APPLICATION_JSON));
        this.getVariants().add(new Variant(MediaType.TEXT_HTML));
        this.getVariants().add(new Variant(MediaType.TEXT_XML));
    }

    @Override
    public boolean allowDelete() {
        try {
            return experiment.canEdit(getUser());
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void handleDelete() {
        //this removes any association between the visit and experiment.
        String unsharedVisit = experiment.getVisit();
        String visitId = visit.getId();
        if (visit.getClosed() != null && visit.getClosed()) {
            getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            getResponse().setEntity("This visit has already been closed and may not have experiments removed from it.", MediaType.TEXT_PLAIN);
            return;
        }

        if (visitId.equalsIgnoreCase(unsharedVisit)) {
            updateVisit("NULL", "NULL", false,
                    "Unable to remove " + experiment.getId() + " from visit " + visit);
        } else {
            if (sharedExpt == null) {
                getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
                getResponse().setEntity("Experiment " + experiment.getId() + "not associated with visit " + visitId,
                        MediaType.TEXT_PLAIN);
                return;
            }
            updateVisit("NULL", "NULL", true,
                    "Unable to remove " + experiment.getId() + " from visit " + visit + " in project " + project.getId());
        }

    }

    @Override
    public boolean allowPut() {
        try {
            return experiment.canEdit(getUser());
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void handlePut() {
        //this will associate an experiment with the visit if possible.
        String projectId = project.getId();
        String visitId = visit.getId();
        if (experiment.getVisit() != null && !experiment.getVisit().equals(visitId)) {
            this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
            getResponse().setEntity("This experiment has already been associated with another visit. It must be unassociated first.", MediaType.TEXT_PLAIN);
            return;
        }
        try {
            String st = experiment.getProtocol();
            if (subtype == null && StringUtils.isNotBlank(st)) {
                // ideally, we want to keep the experiment's subtype with this new visit
                try {
                    ProtocolVisitSubjectHelper.validateExperimentVisit(experiment.getXSIType(), visitId,
                            experiment.getDate(), subject.getId(), st, getUser());
                } catch (ServerException e) {
                    // if experiment's current subtype is not valid for this new visit, remove it
                    ProtocolVisitSubjectHelper.validateExperimentVisit(experiment.getXSIType(), visitId,
                            experiment.getDate(), subject.getId(), subtype, getUser());
                    experiment.setProtocol("NULL");
                }
            } else {
                ProtocolVisitSubjectHelper.validateExperimentVisit(experiment.getXSIType(), visitId,
                        experiment.getDate(), subject.getId(), subtype, getUser());
                experiment.setProtocol(subtype);
            }
        } catch (ClientException | ServerException e) {
            this.getResponse().setStatus(e.getStatus());
            getResponse().setEntity(e.getMessage(), MediaType.TEXT_PLAIN);
            return;
        }

        updateVisit(visitId, null, sharedExpt != null,
                "Unable to add visit " + visitId + " to experiment " + experiment.getId() + " in project " + projectId);
    }

    /**
     * update visit
     * @param visitId the visit id
     * @param protocol the subtype or null if you've already updated subtype on the object
     * @param updateShared true to update sharedExpt, false to update experiment
     * @param msg failure message
     */
    private void updateVisit(String visitId, @Nullable String protocol, boolean updateShared, String msg) {
        try {
            if (updateShared) {
                sharedExpt.setVisit(visitId);
                if (protocol != null) {
                    sharedExpt.setProtocol(protocol);
                }
                EventMetaI c = null;
                SaveItemHelper.authorizedSave(((XnatExperimentdataShare) sharedExpt).getItem(),
                        getUser(), true, false, c);
            } else {
                experiment.setVisit(visitId);
                if (protocol != null) {
                    experiment.setProtocol(protocol);
                }
                experiment.save(getUser(), true, false, null);
            }
            getResponse().setStatus(Status.SUCCESS_OK);
        } catch (Exception e) {
            _log.error(msg, e);
            getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
            getResponse().setEntity(msg + ": " + e.getMessage(), MediaType.TEXT_PLAIN);
        }
    }
}
